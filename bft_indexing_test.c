#include <stdio.h>

#define N 8
#define logN 3

int main() {
	for(int l=1;l<logN;l++) {
		printf("level=%d\n",l);
		for(int m=0;m<N/(1<<(l+1));m++) {
			for(int n=0;n<(1<<(l));n++) {
				int calc=(m*(1<<(l+1)))+n;
				int calc1=(m*(1<<(l+1)))+n+(1<<(l));
				printf("m%d n%d [l_o=%d, u_o=%d]\n",
						m,n,calc,calc1);
			}
		}
		printf("\n");
	}
}
