#!/bin/zsh

#rm -f result_wrap0.txt
#rm -f result_wrap1.txt
##
## 4 * 2 * 7 * 9 * 2 = 1008
#parallel --progress --gnu -j16 --header --group : \
#        '
#        ./wrap_results.sh {#} {WRAP} {TOPO} {N} {RATE} {PAT} '\
#        ::: TOPO TREE MESH0 MESH1 XBAR \
#        ::: PAT 0 1 \
#        ::: N 2 4 8 16 32 64 128 \
#        ::: RATE 1 2 3 4 5 10 20 50 100 \
#        ::: WRAP 0 1 \
#
#exit

# 4 * 2 * 2 * 9 * 2 = 288
parallel --progress --gnu -j32 --header --group : \
        '
        ./wrap_results.sh {#} {WRAP} {TOPO} {N} {RATE} {PAT} '\
        ::: TOPO TREE MESH0 MESH1 XBAR \
        ::: PAT 0 1 \
        ::: N 256 \
        ::: RATE 1 2 3 4 5 10 20 50 100 \
        ::: WRAP 0 1 \

