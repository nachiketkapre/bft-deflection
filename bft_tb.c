#include "Vbft.h"
#include "verilated.h"
#include <iostream>
using namespace std;


int main(int argc, char **argv, char **env) {
        Verilated::commandArgs(argc, argv);
        Vbft* top = new Vbft;
        // TODO: add initialization stuff for generics and add a clock loop
        int time=0;
        bool clk=0;
        top->ce = 1;
        top->rst = 1;
        top->clk = 0;
        top->in = 0;
	top->cmd = 18; // Cmd_RND=18 
        while (!Verilated::gotFinish() && !(top->done_all==1)) {
                top->rst = (time<2);
                top->clk = clk;
                top->eval();
                clk=!clk;
                time++;
        }
//      cout << "// Verilator simulation finished at time=" << time << endl;
        delete top;
        exit(0);
}
