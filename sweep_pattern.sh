#!/bin/zsh

rm -f result_pattern.txt

# 4 * 4 * 1 * 9 * 1 = 144 tests
parallel --progress --gnu -j32 --header --group : \
	'
	./wrap_test.sh {#} {WRAP} {TOPO} {N} {RATE} {PAT} '\
	::: TOPO TREE MESH0 MESH1 XBAR \
	::: PAT 0 1 2 3 \
	::: N 256 \
	::: RATE 1 2 3 4 5 10 20 50 100 \
	::: WRAP 1 \

