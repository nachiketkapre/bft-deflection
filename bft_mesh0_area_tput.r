#!/usr/bin/Rscript

library(lattice)
library(ggplot2)
library(ggthemes)
library(sitools)

get_switch_count <- function (n,topo) {

	if(topo=="TREE") {
		TYPE<-c(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	} else if(topo=="XBAR") {
		TYPE<-c(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
	} else if(topo=="MESH0") {
		TYPE<-c(1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0);
	} else if(topo=="MESH1") {
		TYPE<-c(1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0);
	} else {
		print("Unsupported TOPO");
	}

	LEVELS=log(n)/log(2)
	PI_SWITCHES=vector(,LEVELS);
	T_SWITCHES=vector(,LEVELS);

	for(l in seq(0,LEVELS-1)) {
		PI_SWITCHES[l+1]=n/2;
		T_SWITCHES[l+1]=n/2;
	}
	for(l in seq(0,LEVELS-1)) {
		if(TYPE[l+1]==0) {
			PI_SWITCHES[l+1]=0;
			if(l<=LEVELS-2) {
				for(l1 in seq(l+2,LEVELS)) {
					T_SWITCHES[l1]=T_SWITCHES[l1]/2;
					PI_SWITCHES[l1]=PI_SWITCHES[l1]/2;
				}
			}
		} else if (TYPE[l+1]==1){
			T_SWITCHES[l+1]=0;
		} else {
			print("Error in counting switches");
		}
		print(T_SWITCHES);
	}
	sum_pi=sum(PI_SWITCHES);
	sum_t=sum(T_SWITCHES);
	print(sprintf("n=%d,sum_pi=%d, sum_t=%d\n",n,sum_pi,sum_t));
	c(sum_pi,sum_t);
}

pdf(file="bft_mesh0_area_tput.pdf", height=3, width=4);

df0<-read.csv("result_wrap0.csv",header=TRUE,comment.char='#', row.names=NULL);
df1<-read.csv("result_wrap1.csv",header=TRUE,comment.char='#', row.names=NULL);

df1$wrap<-"1"
df0$wrap<-"0"
df1$wraptxt<-"Loopback"
df0$wraptxt<-"Bounce"

df<-rbind(df0,df1);
df<-df[df$topo=="MESH0",]
df<-df[df$rate==15,]
b<-mapply(get_switch_count,df$n,df$topo)
df$pi<-b[1,]
df$t<-b[2,]

phy<-read.csv("area.csv",header=TRUE,comment.char='#', row.names=NULL);

df<-merge(df,phy);
df$area<-df$pi*df$lut_pi + df$t*df$lut_t;
df$wrap<-df$wraptxt

p <- ggplot(df,aes(y=packets/(time),x=area,group=(wrap)))+
	geom_line(size=1.2,aes(colour=factor(wrap)))+
	geom_point(size=3,aes(colour=factor(wrap),shape=factor(wrap)))+
	xlab("Area (LUTs)")+ylab("Bandwidth (pkt/cyc)")+
	scale_x_log10(labels=f2si)+
	scale_y_continuous(limits=c(0.1,40));
	#scale_y_continuous(
	#	limits=c(0.01,0.5),
	#	breaks=c(0.01,0.1,0.5),
	#	labels=c(0.01,0.1,0.5));

p + theme_economist_white(gray_bg=FALSE) + 
	guides(colour=guide_legend(nrow=1))+
	theme(
		legend.position="top", 
		legend.title=element_blank(),
		legend.background=element_blank(),
		legend.text=element_text(size=10));

