#!/bin/zsh

rm -f result_hw.txt
 
# gather results
# vivado compilation/synthesis
parallel --gnu -j1 --header --group : \
	'
	printf "TOPO={TOPO}, WRAP={WRAP}, N={N}, " >> result_hw.txt | cat
	pushd hw/bft_{TOPO}_{WRAP}_{N};
	lut=`cat vivado.log | grep "Slice LUTs" | cut -d"|" -f 3 | sed "s/ //g"`
	ff=`cat vivado.log | grep "Slice Registers" | cut -d"|" -f 3 | sed "s/ //g"`
	slack=`cat vivado.log | grep "Slack" | cut -d":" -f 2 | cut -d"(" -f 1 | sed "s/ //g" | sed "s/ns//g"`
	popd
	echo $lut","$ff","$slack >> result_hw.txt | cat'\
	::: TOPO XBAR MESH0 MESH1 TREE \
	::: N 2 4 8 16 32 64 128 256 512\
	::: WRAP 0 1 \

