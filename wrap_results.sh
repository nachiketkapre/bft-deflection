#!/bin/zsh

# simply compares all sent message datastamps with received messages to confirm is *all* messages were received
ID=${2}_${3}_${4}_${5}_$6
WRAP=$2
TOPO=$3
N=$4
RATE=$5
PAT=$6

cd bin/$ID

cp ../../result_skel.sh .

echo "sem --id 0 echo ID=$1, WRAP=$2, TOPO=$3, N=$4, RATE=$5, PAT=$6, sent=\$packets_sent, recv=\$packets_recv, time_taken=\$time_taken, injection_rate=\$irtxt, sustained rate=\$ortxt, worst_latency=\$worst_latency, sum_queueing=\$sum_queueing >> ../../results_wrap$2.txt" >> result_skel.sh

tar xvf log.tar.gz

chmod +x result_skel.sh
./result_skel.sh

tar cvzf log.tar.gz *.log
rm -Rf *.log
