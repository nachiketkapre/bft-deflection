#include "Vexample.h"
#include "verilated.h"
#include <iostream>
using namespace std;

int main(int argc, char **argv, char **env) {
        Verilated::commandArgs(argc, argv);
        Vexample* top = new Vexample;
	bool clk=0;
        
	top->in = 0;
	top->clk = clk;
        top->eval();
	clk=!clk;
	cout << top->out << endl;
	
        top->in = 10;
	top->clk = clk;
        top->eval();
	clk=!clk;
	cout << top->out << endl;
	
        top->in = 5;
	top->clk = clk;
        top->eval();
	clk=!clk;
	cout << top->out << endl;
        
	delete top;
        exit(0);
}
