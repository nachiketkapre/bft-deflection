#!/bin/zsh

# simply compares all sent message datastamps with received messages to confirm is *all* messages were received
rm -f result_wrap0.txt
rm -f result_wrap1.txt
mkdir -p ./bin
rm -Rf ./bin/*

# 4 * 2 * 2 * 10 * 6 = 960 tests
parallel --progress --gnu -j32 --header --group : \
	'
	./wrap_test.sh {#} {WRAP} {TOPO} {N} {RATE} {PAT} '\
	::: TOPO TREE MESH0 MESH1 XBAR \
	::: PAT 0 1 \
	::: N 2 4 8 16 32 64 \
	::: RATE 1 2 3 4 5 7.5 10 20 50 100 \
	::: WRAP 0 1 \

# 4 * 2 * 2 * 10 * 2 = 320 tests
parallel --progress --gnu -j16 --header --group : \
	'
	./wrap_test.sh {#} {WRAP} {TOPO} {N} {RATE} {PAT} '\
	::: TOPO TREE MESH0 MESH1 XBAR \
	::: PAT 0 1 \
	::: N 128 256 \
	::: RATE 1 2 3 4 5 7.5 10 20 50 100 \
	::: WRAP 0 1 \

# 4 * 2 * 2 * 10 * 1 = 160 tests
parallel --progress --gnu -j8 --header --group : \
	'
	./wrap_test.sh {#} {WRAP} {TOPO} {N} {RATE} {PAT} '\
	::: TOPO TREE MESH0 MESH1 XBAR \
	::: PAT 0 1 \
	::: N 512 \
	::: RATE 1 2 3 4 5 7.5 10 20 50 100 \
	::: WRAP 0 1 \
