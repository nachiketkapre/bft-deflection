#!/usr/bin/zsh

N=512
LEVELS=`echo "log($N)/log(2)" | wcalc -q`
XDC=bft_linear.xdc

#echo "create_clock -period 2.000 -waveform {0.000 1.000} [get_nets clk]" > $XDC
rm -f $XDC

#TYPE=(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
#TYPE=(1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1)
#TYPE=(1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0)
TYPE=(1 1 0 1 1 0 1 1 0 1 1 0 1 1 0 1 1 0)

#XMAX=222 #VC707 FPGA
XMAX=111 #maybe things can be closer

x1=`echo "(0)*$XMAX/$LEVELS" | wcalc -q`
x2=`echo "(1)*$XMAX/$LEVELS - 1" | wcalc -q`
echo "create_pblock {pblock_level[0]}" >> $XDC
echo "resize_pblock [get_pblocks {pblock_level[0]}] -add {SLICE_X${x1}Y0:SLICE_X${x2}Y349}" >> $XDC
for m in {0..`echo "$N/2 - 1" | wcalc -q`}
do
	string=t_level0
	if [[ $TYPE[1] == 1 ]]; then 
		string=pi_level0
	fi
	#echo $l $m $n $string
	echo "add_cells_to_pblock [get_pblocks {pblock_level[0]}] [get_cells -quiet [list {xs[$m].$string.sb}]]" >> $XDC
	echo "add_cells_to_pblock [get_pblocks {pblock_level[0]}] [get_cells -quiet [list {xs[$m].cli0}]]" >> $XDC
	echo "add_cells_to_pblock [get_pblocks {pblock_level[0]}] [get_cells -quiet [list {xs[$m].cli1}]]" >> $XDC
done

for l in {1..`echo $LEVELS-1 | wcalc -q`}
do
	x1=`echo "($l)*$XMAX/$LEVELS" | wcalc -q`
	x2=`echo "($l+1)*$XMAX/$LEVELS - 1" | wcalc -q`
	echo "create_pblock {pblock_level[$l]}" >> $XDC
	echo "resize_pblock [get_pblocks {pblock_level[$l]}] -add {SLICE_X${x1}Y0:SLICE_X${x2}Y349}" >> $XDC
	for m in {0..`echo "$N/(1<<($l+1)) - 1" | wcalc -q`}
	do
		for n in {0..`echo "(1<<$l) - 1" | wcalc -q`}
		do
			string=t_level
			if [[ $TYPE[`echo $l+1 | wcalc -q`] == 1 ]]; then 
				string=pi_level
			fi
			#echo $l $m $n $string
			echo "add_cells_to_pblock [get_pblocks {pblock_level[$l]}] [get_cells -quiet [list {n2.ls[$l].ms[$m].ns[$n].$string.sb}]]" >> $XDC
		done
	done
done

