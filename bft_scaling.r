#!/usr/bin/Rscript

library(lattice)
library(ggplot2)
library(ggthemes)

pdf(file="bft_scaling.pdf", height=3, width=5);

df0<-read.csv("result_wrap0.csv",header=TRUE,comment.char='#', row.names=NULL);
df1<-read.csv("result_wrap1.csv",header=TRUE,comment.char='#', row.names=NULL);

df1$wrap<-"Loopback"
df0$wrap<-"Bounce"

df<-rbind(df0,df1);
df<-df[df$topo=="MESH1",]
df<-df[df$rate!=1,]
df<-df[df$rate!=5,]
df<-df[df$rate!=10,]
df<-df[df$rate!=20,]
df<-df[df$rate!=50,]

p <- ggplot(df,aes(y=packets/(time*n),x=n,group=(wrap)))+
	facet_grid(.~rate)+
	geom_line(size=1.2,aes(colour=factor(wrap)))+
	geom_point(size=3,aes(colour=factor(wrap),shape=factor(wrap)))+
	xlab("PEs")+ylab("Bandwidth (pkt/cyc/pe)")+
	scale_x_log10() + scale_y_log10();

p + theme_economist_white(gray_bg=FALSE) + 
	guides(colour=guide_legend(nrow=1))+
	theme(
		legend.position="top", 
		legend.title=element_blank(),
		legend.background=element_blank(),
		legend.text=element_text(size=10));

