#!/bin/zsh

# simply compares all sent message datastamps with received messages to confirm is *all* messages were received
cat ./tmp/test.log | grep "Sent" | cut -d" " -f 9 | sed "s/packetid=//" | sed "s/,//" | sort -V > ./tmp/sent.log
cat ./tmp/test.log | grep "Received" | cut -d" " -f 7 | sed "s/data=//" | sed "s/,//" | sort -V > ./tmp/recv.log
diff ./tmp/sent.log ./tmp/recv.log
