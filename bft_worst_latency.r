#!/usr/bin/Rscript

library(lattice)
library(ggplot2)
library(ggthemes)

pdf(file="bft_worst_latency.pdf", height=3, width=4);

df0<-read.csv("result_wrap0.csv",header=TRUE,comment.char='#', row.names=NULL);
df1<-read.csv("result_wrap1.csv",header=TRUE,comment.char='#', row.names=NULL);

df1$wrap<-"Loopback"
df0$wrap<-"Bounce"

df<-rbind(df0,df1);
df<-df[df$topo=="XBAR",]
df<-df[df$n==32,]

p <- ggplot(df,aes(y=worst_latency,x=rate,group=(wrap)))+
	geom_line(size=1.2,aes(colour=factor(wrap)))+
	geom_point(size=3,aes(colour=factor(wrap),shape=factor(wrap)))+
	xlab("Injection Rate")+ylab("Worst Latency")+
	scale_x_log10() + scale_y_log10();

p + theme_economist_white(gray_bg=FALSE) + 
	guides(colour=guide_legend(nrow=1))+
	theme(
		legend.position="top", 
		legend.title=element_blank(),
		legend.background=element_blank(),
		legend.text=element_text(size=10));

