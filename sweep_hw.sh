#!/bin/zsh

# vivado compilation/synthesis
parallel --gnu -j8 --header --group : \
	'
	echo "ID={#}, WRAP={WRAP} TOPO={TOPO}, N={N}, " 
	mkdir -p hw/bft_{TOPO}_{WRAP}_{N}
	pushd hw/bft_{TOPO}_{WRAP}_{N}
	pwd
	echo "create_clock -period 2.000 -waveform {0.000 1.000} [get_nets clk]" > bft.xdc
	echo "read_verilog ../../bft.v ../../t_switch.v ../../pi_switch.v ../../t_route_lean.v ../../pi_route_lean.v ../../t_route.v ../../pi_route.v ../../mux.v ../../client_for_synth.v;" > compile.tcl
	echo "read_xdc bft.xdc;" >> compile.tcl
	echo "synth_design -mode out_of_context -part xc7vx485tffg1761-2 -verilog_define {TOPO}=1 -generic N={N} -generic WRAP={WRAP} -generic PAT=0 -top bft;"  >> compile.tcl
	echo "write_checkpoint -force -file bft_synth.dcp" >> compile.tcl 
	echo "opt_design; place_design; route_design; report_utilization; report_timing;"  >> compile.tcl
	echo "write_checkpoint -force -file bft_place-and-route.dcp" >> compile.tcl
	echo "exit;" >> compile.tcl
	vivado -mode tcl -s compile.tcl
	popd'\
	::: TOPO MESH0 MESH1 XBAR TREE \
	::: N 2 4 8 16 32 64 128 \
	::: WRAP 0 1 \

	#::: N 2 4 8 16 32 64 128 256 512\
