#!/bin/zsh

# simply compares all sent message datastamps with received messages to confirm is *all* messages were received
ID=${2}_${3}_${4}_${5}_$6
WRAP=$2
TOPO=$3
N=$4
RATE=$5
PAT=$6

#rm -f a_$1.out
# order matters -- system_$1.v must go first..
#iverilog system_$1.v bft_tb.v bft.v client.v mux.v pi_switch.v t_switch.v t_route.v t_route_lean.v pi_route.v pi_route_lean.v -o a_$1.out &> /dev/null
#./a_$1.out > $TMPDIR/test_$1.log

mkdir -p bin/$ID
cd bin/$ID
cp ../../*.c .
cp ../../*.v .
cp ../../*.h .

cat > compile.sh << EOF
verilator -Wno-WIDTH -Wno-WIDTHCONCAT -D$TOPO -GWRAP=$WRAP -GN=$N -GRATE=$RATE -GPAT=$PAT -cc bft.v client.v mux.v pi_switch.v t_switch.v t_route.v t_route_lean.v pi_route.v pi_route_lean.v --exe bft_tb.c
make -C obj_dir -j -f Vbft.mk Vbft
./obj_dir/Vbft > test.log
cat test.log | grep "Sent" | cut -d" " -f 9 | sed "s/packetid=//" | sed "s/,//" | sort -V > sent.log
cat test.log | grep "Received" | cut -d" " -f 7 | sed "s/data=//" | sed "s/,//" | sort -V > recv.log
diff sent.log recv.log
EOF

chmod +x compile.sh
./compile.sh

tar cvzf log.tar.gz *.log
rm -Rf *.log
rm -Rf obj_dir
