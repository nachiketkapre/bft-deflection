#!/bin/zsh

# vivado compilation/synthesis
parallel --gnu -j8 --header --group : \
	'
	echo "ID={#}, WRAP={WRAP} TOPO={TOPO}, N={N}, " 
	mkdir -p hw/bft_{TOPO}_{WRAP}_{N}
	pushd hw/bft_{TOPO}_{WRAP}_{N}
	pwd
	cp ../../*.v .
	cp ../../*.h .
	yosys -p "read_verilog -mem2reg -D{TOPO} bft.v t_switch.v pi_switch.v t_route_lean.v pi_route_lean.v t_route.v pi_route.v mux.v client_for_synth.v; chparam -set N {N} bft; chparam -set WRAP {WRAP} bft; proc; opt; techmap; opt; synth_xilinx -edif bft.edif"
	echo "create_clock -period 2.000 -waveform {0.000 1.000} [get_nets clk]" > bft.xdc
	echo "read_edif bft.edif;" > compile.tcl
	echo "read_xdc bft.xdc;" >> compile.tcl
	echo "link_design -mode out_of_context -part xc7vx485tffg1761-2 -top bft;"  >> compile.tcl
	echo "write_checkpoint -force -file bft_synth.dcp" >> compile.tcl 
	echo "opt_design; place_design; route_design; report_utilization; report_timing;"  >> compile.tcl
	echo "write_checkpoint -force -file bft_place-and-route.dcp" >> compile.tcl
	echo "exit;" >> compile.tcl
	vivado -mode tcl -s compile.tcl
	popd'\
	::: TOPO MESH0 MESH1 XBAR TREE \
	::: N 2 4 8 16 32 64 128 256 512\
	::: WRAP 0 1 \

