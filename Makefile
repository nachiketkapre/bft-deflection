
all: a.out
	mkdir -p ./tmp
	./a.out > ./tmp/test.log

a.out: *.v *.h
	rm system.h 
	touch system.h
	iverilog -g2012 -DTREE bft_tb.v bft.v client.v t_switch.v pi_switch.v mux.v pi_route.v t_route.v pi_route_lean.v t_route_lean.v

run: wrap_test.sh a.out
	./wrap_test.sh 1

clean:
	rm -Rf a_*.out
