#!/usr/bin/Rscript

library(lattice)
library(ggplot2)
library(ggthemes)

pdf(file="bft_rate_compare_deflection.pdf", height=3, width=4);

df<-read.csv("result_wrap0.csv",header=TRUE,comment.char='#', row.names=NULL);
df<-df[df$n==256,]
df<-na.omit(df)

p <- ggplot(df,aes(y=packets/(time*n),x=rate,group=(topo)))+
	geom_line(size=0.5,aes(colour=factor(topo)))+
	geom_point(size=3,aes(colour=factor(topo),shape=factor(topo)))+
	xlab("Injection Rate")+ylab("Bandwidth (pkt/cyc/pe)")+
	scale_x_log10();

p + theme_economist_white(gray_bg=FALSE) + 
	guides(colour=guide_legend(nrow=1))+
	theme(
		legend.position="top", 
		legend.title=element_blank(),
		legend.background=element_blank(),
		legend.text=element_text(size=10));

