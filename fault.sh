#!/bin/zsh

pushd bin
for i in *; do echo $i `diff $i/sent.log $i/recv.log | wc -l`; done;
popd
