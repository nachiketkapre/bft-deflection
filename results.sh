#!/bin/zsh

# gather results
cat results_wrap0.txt | cut -d" " -f 4,5,6,8,9,13,14 | sed "s/[^0-9,.]//g" > /tmp/data.csv
cat results_wrap0.txt | cut -d" " -f 3 | sed "s/.*=//g" | sed "s/,//g" > /tmp/topo.csv
echo "topo,n,rate,pattern,packets,time,worst_latency,sum_queueing" > results_wrap0.csv
paste -d"," /tmp/topo.csv /tmp/data.csv >> results_wrap0.csv

cat results_wrap1.txt | cut -d" " -f 4,5,6,8,9,13,14 | sed "s/[^0-9,.]//g" > /tmp/data.csv
cat results_wrap1.txt | cut -d" " -f 3 | sed "s/.*=//g" | sed "s/,//g" > /tmp/topo.csv
echo "topo,n,rate,pattern,packets,time,worst_latency,sum_queueing" > results_wrap1.csv
paste -d"," /tmp/topo.csv /tmp/data.csv >> results_wrap1.csv
